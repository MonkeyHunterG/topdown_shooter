extends KinematicBody2D
class_name Player


export (int) var player_speed = 200


onready var health_res: Node2D = $Health_node2D
onready var weapon_res: Weapon = $Weapon_node2D
onready var team_res = $Team_node2D


func _ready():
	weapon_res.initialize(team_res.team)


func _physics_process(_delta: float) -> void:
	var movement_direction := Vector2.ZERO
	movement_direction.x = Input.get_axis("ui_left", "ui_right")
	movement_direction.y = Input.get_axis("ui_up", "ui_down")

	movement_direction = movement_direction.normalized()
	move_and_slide(movement_direction * player_speed)
	
	look_at(get_global_mouse_position())


# Isn't called every frame like _process
# Is only called when there's input for the Player that isn't handled yet
func _unhandled_input(event: InputEvent) -> void:
	if(event.is_action_pressed("left_mouse_clicked")):
		weapon_res.shoot()


func handle_hit():
	health_res.health -= 1
	if(health_res.health < 1):
		queue_free()


func get_team() -> int:
	return team_res.team
