extends KinematicBody2D


export (int) var speed = 100


onready var health_res: Node2D = $Health_node2D
onready var ai_res: Node2D = $AI_node2D
onready var weapon_res: Weapon = $Weapon_node2D
onready var team_res = $Team_node2D


func _ready():
	# dependency injection links to not directly related child nodes
	# uses parent to link them
	ai_res.initialize(self, weapon_res, team_res.team)
	weapon_res.initialize(team_res.team)


func handle_hit():
	health_res.health -= 1
	if(health_res.health < 1):
		queue_free()


func rotate_toward(target_location: Vector2) -> void:
	var target_angle: float = global_position.direction_to(target_location).angle()
	rotation = lerp_angle(rotation, target_angle, 0.1)


func velocity_toward(target_location: Vector2) -> Vector2:
	return global_position.direction_to(target_location) * speed


func get_team() -> int:
	return team_res.team
