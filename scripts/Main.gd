extends Node2D


onready var bullet_manager: Node2D = $BulletManager
onready var player: Player = $Player


func _ready():
	# randomizes all seeds for random number generators
	# e.g. patrolling enemies won't use same path after restart
	randomize()
	# to connect two child nodes, connect 
	# from node that emits signal 
	# to node that has appropriate func
	# player -> bullet_manager
	GlobalSignals.connect("bullet_fired", bullet_manager, "handle_bullet_spawned")
