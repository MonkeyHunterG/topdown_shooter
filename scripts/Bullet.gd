extends Area2D
# adding the class_name allows for auto-completion when strongly typing parameters
# like in BulletManager
class_name Bullet


export (int) var bullet_speed = 10


onready var kill_timer: Timer = $Kill_Bullets_Timer


var bullet_direction = Vector2.ZERO
var team: int = -1


func _ready() -> void:
	kill_timer.start()


func _physics_process(_delta: float) -> void:
	if(bullet_direction != Vector2.ZERO):
		var velocity = bullet_direction * bullet_speed
		
		global_position += velocity


func set_direction(direction: Vector2):
	self.bullet_direction = direction
	rotation += direction.angle()


func _on_Kill_Bullets_Timer_timeout() -> void:
	queue_free()


func _on_Bullet_area2d_body_entered(body: Node) -> void:
	if(body.has_method("handle_hit")):
		if(body.has_method("get_team") && body.get_team() != self.team):
			body.handle_hit()
		queue_free()
