extends Node2D


export (int) var health = 1 setget set_health


func set_health(value: int):
	health = clamp(value, 0, 20)
