extends Node


# Project Settings -> Autoload -> add Path
# now every script can use this signal, no matter where it is
signal bullet_fired(bullet, team, position, direction)
