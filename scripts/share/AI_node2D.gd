extends Node2D


signal state_changed(new_state)


onready var player_detection_area: Area2D = $Detection_area2D
onready var patrol_timer = $Patrol_timer


# setting to an invalid state for the ready function to appropriately set
var current_state = -1 setget set_state
var team = -1


# general variables
const SPEED = 100


# ENGAGE state variables
var target: KinematicBody2D = null
var weapon: Weapon = null
var actor: KinematicBody2D = null


# PATROL state variables
const PATROL_RANGE = 50
var origin: Vector2 = Vector2.ZERO
var patrol_destination: Vector2 = Vector2.ZERO
var patrol_destination_reached: bool = false
var actor_velocity: Vector2 = Vector2.ZERO


# 2 states: when ai sees or doesn't see player
enum State {
	PATROL,
	ENGAGE
}


func _ready() -> void:
	set_state(State.PATROL)


func _process(_delta: float) -> void:
	match current_state:
		State.PATROL:
			if(!patrol_destination_reached):
				actor.rotate_toward(patrol_destination)
				actor.move_and_slide(actor_velocity)
				if(actor.global_position.distance_to(patrol_destination) < 5):
					patrol_destination_reached = true
					actor_velocity = Vector2.ZERO
					patrol_timer.start()
		State.ENGAGE:
			if(target != null and weapon != null):
				actor.rotate_toward(target.position)
				var angle_to_player: float = actor.global_position.direction_to(target.position).angle()
				shoot_player(angle_to_player)
			else:
				print("Engage, but no target/weapon")
		_: # Godots default else-case
			print("Error State")


## Shoots player, if actor faces the general direction of the player
func shoot_player(angle_to_player: float) -> void:
	if(abs(actor.rotation) - abs(degree_to_radians(angle_to_player)) < 0.1):
		weapon.shoot()


func degree_to_radians(angle: float) -> float:
	return angle * 180 / PI


func initialize(new_actor: KinematicBody2D, new_weapon: Node2D, new_team: int) -> void:
	actor = new_actor
	weapon = new_weapon
	team = new_team


func set_state(new_state: int) -> void:
	if(current_state == new_state):
		return
	if(new_state == State.PATROL):
		origin = global_position
		patrol_timer.start()
		patrol_destination_reached = true
	current_state = new_state
	emit_signal("state_changed", current_state)


func _on_Detection_area2D_body_entered(body: PhysicsBody2D):
	if(body.has_method("get_team") && body.get_team() != team):
		set_state(State.ENGAGE)
		target = body


func _on_Detection_area2D_body_exited(body: PhysicsBody2D):
	if(target and body == target):
		set_state(State.PATROL)
		target = null


func _on_Patrol_timer_timeout():
	var random_x = rand_range(-PATROL_RANGE, PATROL_RANGE)
	var random_y = rand_range(-PATROL_RANGE, PATROL_RANGE)
	patrol_destination = Vector2(random_x, random_y) + origin
	patrol_destination_reached = false
	actor_velocity = actor.velocity_toward(patrol_destination)
