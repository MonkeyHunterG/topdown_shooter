extends Node2D
class_name Weapon


export (PackedScene) var Bullet


onready var end_of_gun: Position2D = $EndOfGun_position2D
onready var gun_direction: Position2D = $GunDirection_position2D
onready var attack_cooldown_timer: Timer = $AttackCooldown_timer
onready var muzzle_flash_animation: AnimationPlayer = $AnimationPlayer


var team: int = -1


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func initialize(new_team: int) -> void:
	team = new_team


func shoot():
	# Timer starts at stopped and Bullet is instanced
	if(attack_cooldown_timer.is_stopped() and Bullet != null):
		muzzle_flash_animation.play("muzzle_flash_animation")
		var bullet_instance: Area2D = Bullet.instance()
		# Vector math to get travel vector
		var direction: Vector2 = (gun_direction.global_position - end_of_gun.global_position).normalized()
		GlobalSignals.emit_signal("bullet_fired", bullet_instance, team, end_of_gun.global_position, direction)
		# Timer is set as one_shot, therefore needs restart
		attack_cooldown_timer.start()
